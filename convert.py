import csv
import json
import calendar
from datetime import datetime

def convert_time_format(time_str):
    # Convert time from 12-hour format to 24-hour format
    dt = datetime.strptime(time_str, '%H:%M')
    return dt.strftime('%I:%M %p').replace("\"", "")

def process_csv(csv_file):
    with open(csv_file, 'r') as file:
        csv_reader = csv.DictReader(file)
        data = {}
        monthdata = {}
        for row in csv_reader:
            date = datetime.strptime(row['d_date'], '%m/%d/%Y')
            day = date.day

            if day not in data:
                data[day] = {
                    "Fajr": convert_time_format(row['fajr_begins']),
                    "Sunrise": convert_time_format(row['sunrise']),
                    "Dhuhr": convert_time_format(row['zuhr_begins']),
                    "Asr": convert_time_format(row['asr_mithl_1']),
                    "Maghrib": convert_time_format(row['maghrib_begins']),
                    "Ishaa": convert_time_format(row['isha_begins']),
                    "Midnight": "1:30 AM"
                }
                if date == datetime.strptime('12/31/2024', '%m/%d/%Y'):
                    monthdata[calendar.month_name[date.month]] = data
                    return monthdata
            else:
                monthdata[calendar.month_name[date.month-1]] = data
                data = {}
                data[day] = {
                    "Fajr": convert_time_format(row['fajr_begins']),
                    "Sunrise": convert_time_format(row['sunrise']),
                    "Dhuhr": convert_time_format(row['zuhr_begins']),
                    "Asr": convert_time_format(row['asr_mithl_1']),
                    "Maghrib": convert_time_format(row['maghrib_begins']),
                    "Ishaa": convert_time_format(row['isha_begins']),
                    "Midnight": "1:30 AM"
                }
    return monthdata

def save_json(data, month):
    json_data = json.dumps(data, indent=2)
    with open(f'{month}.json', 'w') as json_file:
        json_file.write(json_data)

def main():
    csv_file = 'salah.csv'  # Update with your actual CSV file name
    data = process_csv(csv_file)

    for month, data in data.items():
        save_json(data,month)


if __name__ == "__main__":
    main()
