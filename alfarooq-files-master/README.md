### Adhaan format

JSON object

One JSON object for each day of the month file:

```
{ 
    "1": {
	   "Fajr": "5:17 AM",
		"Sunrise": "6:49 AM",
		"Dhuhr": "1:35 PM",
		"Asr": "5:17 PM",
		"Maghrib": "8:24 PM",
		"Ishaa": "9:48 PM",
		"Midnight": "1:43 AM"
    },
    "2": {
	   "Fajr": "5:17 AM",
		"Sunrise": "6:49 AM",
		"Dhuhr": "1:35 PM",
		"Asr": "5:17 PM",
		"Maghrib": "8:24 PM",
		"Ishaa": "9:48 PM",
		"Midnight": "1:43 AM"
    }
}
```

### Iqaamah format

JSON array